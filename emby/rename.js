// 设置下载文件名称
if ($request.url.indexOf('/Videos/') != -1) {
  if($response.status==200){
    let query = getQueryVariable($request.url);
    $response.headers['Content-Disposition'] = 'attachment;filename=' + decodeURI(query.filename);
    $done({
      status: 200,
      headers: $response.headers
    })
  }else{
    $done({});
  }
}

// 获取 URL 参数
function getQueryVariable(url){
  let index=url.lastIndexOf('?');
  let query = url.substring(index+1, url.length);
  let vars = query.split("&");
  let querys = new Object();
  for (let i=0; i<vars.length; i++) {
    let pair = vars[i].split("=");
    querys[pair[0]] = pair[1]
  }
  if (Object.keys(querys).length == 0) {
    return null;
  }else{
    return querys;
  }
}
