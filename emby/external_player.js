// 调用外部播放器
if ($request.url.indexOf('/PlaybackInfo') != -1 && $request.method == 'POST' && $response.status==200){
  let host = getHost($request.url);
  //let video_id = $request.url.match(/emby\/Items\/(\S*)\/PlaybackInfo/)[1]; // 视频 ID
  let query = getQueryVariable($request.url);
  //let user_id = query.UserId; // 用户 ID
  //let media_source_id = query.MediaSourceId;  // 媒体源 ID
  let subtitle_stream_index = query.SubtitleStreamIndex;  // 字幕索引
  let is_playback = query.IsPlayback;   // 是否播放
  //let X_emby_token = query['X-Emby-Token'];   // Emby 令牌
  //let X_emby_device_id = query['X-Emby-Device-Id'];   // Emby 设备 ID
  //let X_emby_device_name = query['X-Emby-Device-Name'];   // Emby 设备 ID
  //let X_emby_client = query['X-Emby-Client'];   // Emby 客户端名称
  //let X_emby_client_version = query['X-Emby-Client-Version'];   // Emby 客户端版本
  let x_callback_url = stream_url = subtitle_url = '';

  if(is_playback == 'true') { // 点播放按钮播放的时候
    let body = JSON.parse($response.body);   // 响应消息体
    for (let media_sources_index in body.MediaSources) {
      if (media_sources_index == 0) { // 只取第一个
        stream_url = host+body.MediaSources[media_sources_index].DirectStreamUrl;    // 获取影片地址
        subtitle_url = '';  // 获取字幕地址
        for (let media_streams_index in body.MediaSources[media_sources_index].MediaStreams){
          // 获取字幕地址
          if (body.MediaSources[media_sources_index].MediaStreams[media_streams_index].Type == 'Subtitle' && body.MediaSources[media_sources_index].MediaStreams[media_streams_index].IsExternal == true) {
            if (typeof(subtitle_stream_index) !== 'undefined') {    // 如果选择了外挂字幕
              if(body.MediaSources[media_sources_index].MediaStreams[media_streams_index].Index == subtitle_stream_index) { // 则查找选择的外挂字幕
                subtitle_url = host+body.MediaSources[media_sources_index].MediaStreams[media_streams_index].DeliveryUrl;
                break;
              }
            }else{  // 没有选择外挂字幕，使用搜索到的第一个外挂字幕
              subtitle_url = host+body.MediaSources[media_sources_index].MediaStreams[media_streams_index].DeliveryUrl;
              break;
            }
          }
        }
        break;
      }
    }

    x_callback_url = generateXCallbackUrl(stream_url, subtitle_url);
    // $persistentStore.write('x-callback-url', x_callback_url);
    console.log("x-callback-url 地址: " + x_callback_url + "\n");
    $done({status: 301, headers: {Location:x_callback_url} });
  }else{
    $done({});
  }
}else{
  $done({});
}

// 获取 URL 参数
function getQueryVariable(url){
  let index=url.lastIndexOf('?');
  let query = url.substring(index+1, url.length);
  let vars = query.split("&");
  let querys = new Object();
  for (let i=0; i<vars.length; i++) {
    let pair = vars[i].split("=");
    querys[pair[0]] = pair[1]
  }
  if (Object.keys(querys).length == 0) {
    return null;
  }else{
    return querys;
  }
}

// 获取 主机地址
function getHost(url) {
  let index=url.lastIndexOf('/emby/');
  let host = url.substring(0, index);
  if (host.length == 0) {
    return null;
  }else{
    return host;
  }
}

// 生成 x-callback-url
function generateXCallbackUrl() {
  let stream_url = arguments[0] ? encodeURIComponent(arguments[0]) : "";
  let subtitle_url = arguments[1] ? encodeURIComponent(arguments[1]) : "";
  return "vlc-x-callback://x-callback-url/stream?url=" + stream_url + "&sub=" + subtitle_url;
}
