var ShuAppDownload = true;  // 是否使用 iOS 上的 Shu APP 下载，使用的话支持同时下载外挂字幕和文件重命名，不使用的话只能下载视频。 是:true 否:false


// 激活下载按钮
if ($request.url.indexOf('/emby/Users/') != -1) {
  if($response.status==200){
    $response.body = $response.body.replace(/"CanDownload":false,/g, '"CanDownload":true,');
    $response.body = $response.body.replace(/"EnableContentDownloading":false,/g, '"EnableContentDownloading":true,');

    let body = JSON.parse($response.body);

    // 保存用户 ID
    let user_id_result = $request.url.match(/\/emby\/Users\/(\w{32})/);
    if (typeof(user_id_result) != "undefined") {
      $persistentStore.write(user_id_result[1], 'user_id');
    }
    
    // 保存设备信息
    let query = getQueryVariable($request.url);
    $persistentStore.write(query['X-Emby-Client'], 'X-Emby-Client');
    $persistentStore.write(query['X-Emby-Device-Name'], 'X-Emby-Device-Name');
    $persistentStore.write(query['X-Emby-Device-Id'], 'X-Emby-Device-Id');
    $persistentStore.write(query['X-Emby-Client-Version'], 'X-Emby-Client-Version');
    $persistentStore.write(query['X-Emby-Token'], 'X-Emby-Token');

    $done({status: 200, headers: $response.headers, body: JSON.stringify(body) });
  }else{
    $done({});
  }
}

// 重定向下载连接
if ($request.url.indexOf('/Download') != -1){
  if($response.status==401){  // 没有下载权限时
    let user_id = $persistentStore.read('user_id');
    let X_Emby_Client = $persistentStore.read('X-Emby-Client');
    let X_Emby_Device_Name = $persistentStore.read('X-Emby-Device-Name');
    let X_Emby_Device_Id = $persistentStore.read('X-Emby-Device-Id');
    let X_Emby_Client_Version = $persistentStore.read('X-Emby-Client-Version');
    let X_Emby_Token = $persistentStore.read('X-Emby-Token');
    let X_Emby_Authorization = "MediaBrowser Device=\""+X_Emby_Device_Name+"\", DeviceId=\""+X_Emby_Device_Id+"\", Version=\""+X_Emby_Client_Version+"\", Client=\""+X_Emby_Client+"\", Token=\""+X_Emby_Token+"\"";

    let host = getHost($request.url);
    let query = getQueryVariable($request.url);
    let video_id = $request.url.match(/emby\/Items\/(\S*)\/Download/)[1];
    let api_key = query.api_key;
    let media_source_id = query.mediaSourceId;

    // 获取影片信息
    let video_info_url = host + '/emby/Users/' + user_id + '/Items/' + video_id;
    $httpClient.get({
      url: video_info_url,
      headers: {
          "X-Emby-Authorization": X_Emby_Authorization,
      },
    }, function(error, response, data){
      if (error) {
        console.log(error);
        $notification.post("影片信息获取失败️", "", error);
        $done();
      }else{
        let video_data = JSON.parse(data);

        for (let key in video_data.MediaSources) {
          let media_source = video_data.MediaSources[key];
          if (media_source.Id == media_source_id) {  // 找出要下载的视频
            // 获取下载信息
            let download_info = downloadInfo(host, video_id, media_source);

            // 生成 CURL 下载命令
            let command = generateCURL(download_info, X_Emby_Authorization);
            console.log("《" + video_data.SortName + "》 CURL 批量下载命令: " + command + "\n");
            // $notification.post("《" + video_data.SortName + "》 CURL 批量下载命令已生成", "详情请查看日志", command);

            if ( ShuAppDownload == true ) {
              // 使用 Shu 下载
              let shu_download_url = generateShuURL(download_info, api_key);
              console.log("《" + video_data.SortName + "》 Shu 批量下载地址: " + shu_download_url + "\n");
              $done({status: 301, headers: {Location:shu_download_url} })
            } else {
              // 重定向下载连接
              let video_download_url = download_info.video.url + "&api_key=" + api_key;
              console.log("《" + video_data.SortName + "》 视频下载地址: " + video_download_url + "\n");
              $done({
                status: 301,
                headers: {
                  'Location': video_download_url + '&filename=' + download_info.video.filename
                }
              })
            }

            break;
          }
        }
          
        $done({});
      }
    });

  }
}

// 构造下载信息
function downloadInfo (host, video_id, media_source) {
  let video = new Object();
  video.url = host + '/Videos/'+ video_id +'/stream?mediaSourceId=' + media_source.Id + '&static=true';
  video.filename = getFileName(media_source.Path);

  let subtitles = new Array();
  let array_index = 0;
  for (let key in media_source.MediaStreams) {
    let media_streams = media_source.MediaStreams[key];
    if (media_streams.Type == 'Subtitle' && media_streams.IsExternal == 1 && media_streams.IsTextSubtitleStream == 1 ) {
      let subtitle = new Object();
      subtitle.url = host + '/Videos/'+ video_id +'/' + media_source.Id + '/Subtitles/' + media_streams.Index + '/0/Stream.' + media_streams.Codec;
      subtitle.filename = getFileName(media_streams.Path);
      subtitles[array_index] = subtitle;
      array_index++;
    }
  }

  return {
    "video": video,
    "subtitles": subtitles,
  }
}

// 生成 CURL 命令
function generateCURL(data, X_Emby_Authorization) {
  let user_agent = "Emby/2 CFNetwork/1220.1 Darwin/20.3.0";
  let command = "curl -A '" + user_agent + "' -H 'X-Emby-Authorization: " + X_Emby_Authorization + "' -H 'Accept: */*' ";
  command += "-o '" + data.video.filename + "' " + "'" + data.video.url + "' ";

  for (let key in data.subtitles) {
    command += "-o '" + data.subtitles[key].filename + "' " + "'" + data.subtitles[key].url + "' ";
  }

  return command;
}

// 生成 Shu URL
function generateShuURL(data, api_key) {
  let user_agent = "Emby/2 CFNetwork/1220.1 Darwin/20.3.0";
  let urls = new Array();
  urls[0] = {
    'header': {
      'User-Agent': user_agent,
    },
    'url': data.video.url + "&api_key=" + api_key,
    'name': data.video.filename,
    'suspend': false,
  };

  for (let key in data.subtitles) {
    urls.push({
      'header': {
        'User-Agent': user_agent,
      },
      'url': data.subtitles[key].url + "?api_key=" + api_key,
      'name': data.subtitles[key].filename,
      'suspend': false,
    });
  }

  return 'shu://gui.download.http?urls=' + encodeURIComponent(JSON.stringify(urls));
}

// 获文件名
function getFileName(path) {
  return path.substring(path.lastIndexOf('/') + 1);
}

// 获取 URL 参数
function getQueryVariable(url){
  let index=url.lastIndexOf('?');
  let query = url.substring(index+1, url.length);
  let vars = query.split("&");
  let querys = new Object();
  for (let i=0; i<vars.length; i++) {
    let pair = vars[i].split("=");
    querys[pair[0]] = pair[1]
  }
  if (Object.keys(querys).length == 0) {
    return null;
  }else{
    return querys;
  }
}

// 获取 主机地址
function getHost(url) {
  let index=url.lastIndexOf('/emby/');
  let host = url.substring(0, index);
  if (host.length == 0) {
    return null;
  }else{
    return host;
  }
}
